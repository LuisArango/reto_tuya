package userinterfaces;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://automationpractice.com/index.php")
public class SearchUserInterface extends PageObject {
    public static final Target TXT_SEARCH = Target.the("usuario").locatedBy("//input[@name='search_query']");
    public static final Target BTN_SUBMIT = Target.the("usuario").locatedBy("//button[@name='submit_search']");
  

    public static final Target TXT_LOWER_PRICE = Target.the("usuario").locatedBy("//div[@id='left_column']//ul[@class='block_content products-block']/*[{0}]//span[@class='price']");
    
    public static final Target CHOOSE_LOWER_PRICE = Target.the("usuario").locatedBy("//*[contains(text(),'{0}')]/ancestor::div[2]//*[@class='product-name']");
    
  
    public static final Target VALUE = Target.the("usuario").locatedBy("//*[@id='our_price_display']");
    
    
  
}
