package utils;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

import java.util.ArrayList;
import java.util.List;


public class LessPrice {
	static double value=0;
	static int position=0;
	static List<String> resultado =new ArrayList<String>();
	

	
	public static String lessPrice(List<String> list1) 
	{
		value=Double.parseDouble(list1.get(0).replace("$", ""));
		for (int i = 0; i < list1.size(); i++) {
			
				if (Double.parseDouble(list1.get(i).replace("$", ""))<value) {
					value=(Double.parseDouble(list1.get(i).replace("$", "")));
					position=i;
			}
		}
	   theActorInTheSpotlight().remember("value",list1.get(position));  
	   return String.valueOf(value);
	}
	
}
