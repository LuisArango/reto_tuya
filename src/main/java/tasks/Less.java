package tasks;
import static userinterfaces.SearchUserInterface.CHOOSE_LOWER_PRICE;

import java.util.ArrayList;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import utils.LessPrice;

public class Less implements Task {
	private ArrayList<String> values;
	
	public Less(ArrayList<String> values ) {
		this.values=values;
	}

    @Override
    public <T extends Actor> void performAs(T actor) {
      actor.attemptsTo(Click.on(CHOOSE_LOWER_PRICE.of(LessPrice.lessPrice(values))));
    }

    public static Less clothing(ArrayList<String> values ) {
        return Tasks.instrumented(Less.class,values);
    }

}
