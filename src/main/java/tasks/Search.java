package tasks;

import static userinterfaces.SearchUserInterface.*;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Search implements Task {
	private String word;
	
	public Search (String word) 
	{
		this.word=word;
	}

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
        		Enter.theValue(word).into(TXT_SEARCH),
        		Click.on(BTN_SUBMIT));
    }

    public static Search clothing(String word) {

        return Tasks.instrumented(Search.class,word);
    }

}
