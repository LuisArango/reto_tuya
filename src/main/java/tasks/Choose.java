package tasks;

import static userinterfaces.SearchUserInterface.TXT_LOWER_PRICE;

import java.util.ArrayList;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

public class Choose implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
    boolean sw=false;
    int count=1;
	ArrayList<String> values = new ArrayList<String>();

     do {
		if (TXT_LOWER_PRICE.of(String.valueOf(count)).resolveFor(actor).isVisible()) {
	    	values.add(TXT_LOWER_PRICE.of(String.valueOf(count)).resolveFor(actor).getText());	
	    	count++;
			
		}else 
		{
			sw=true;
		}

	  } while (!sw);
    	
     System.out.println(values.toString());
     actor.attemptsTo(Less.clothing(values));
     }

    public static Choose lowerPrice() {

        return Tasks.instrumented(Choose.class);
    }

}
