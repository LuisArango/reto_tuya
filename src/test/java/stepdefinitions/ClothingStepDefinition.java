package stepdefinitions;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import questions.Answer;
import tasks.Choose;
import tasks.OpenHome;
import tasks.Search;

public class ClothingStepDefinition {
	
	@Managed(driver="Chrome")
	private WebDriver driver; 
	
    @Before
    public void DoSomethingBefore(){
        OnStage.setTheStage(new OnlineCast());
    }

	@Given("^the user looks for the web page$")
	public void theUserLooksForTheWebPage() {
		   theActorCalled("Lore");
	        theActorInTheSpotlight().wasAbleTo(OpenHome.browser());	}

	@When("^search for the (.*)$")
	public void searchForTheGarment(String word) {
		  theActorInTheSpotlight().attemptsTo(Search.clothing(word));

	}

	@When("^choose the lowest value$")
	public void chooseTheLowestValue() {
		theActorInTheSpotlight().attemptsTo(Choose.lowerPrice());
	}
	
	@Then("^you will see the lowest price$")
	public void youWillSeeTheLowestPrice() {
		   theActorInTheSpotlight().should(GivenWhenThen
	                .seeThat(Answer.toThe(),Matchers.equalTo(theActorInTheSpotlight().recall("value"))));
	}

}
